package com.globant.test.evaluation;

import java.util.HashSet;
import java.util.Set;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AppTest {
	private static final Logger LOGGER = LoggerFactory.getLogger(AppTest.class);

	@Test
	public void testCelebrityNotFoundWhenEmptyList() {

		LOGGER.info("testCelebrityNotFoundWhenEmptyList");
		Person p = app.findCelebrity(new HashSet<Person>());
		Assert.assertNull(p);
	}

	@Test
	public void testCelebrityNotFoundWhenItKnowsAtLeastOnePerson() {

		two.addNewKnownPerson(one);
		three.addNewKnownPerson(one);
		four.addNewKnownPerson(one);

		one.addNewKnownPerson(two);

		Person celebrity = app.findCelebrity(allPeoplelist);
			
		Assert.assertNull(celebrity);		
	}

	@Test
	public void testCelebrityNotFoundWhenSomeoneDoesntKnowIt() {

		two.addNewKnownPerson(one);
		three.addNewKnownPerson(one);

		Person celebrity = app.findCelebrity(allPeoplelist);
					
		Assert.assertNull(celebrity);
		
	}

	@Test
	public void testCelebrityFoundWhenEveryoneKnowsItAndItDoesntKnowByAnybody() {
		two.addNewKnownPerson(one);
		three.addNewKnownPerson(one);
		four.addNewKnownPerson(one);

		Person celebrity = app.findCelebrity(allPeoplelist);
		
		Assert.assertEquals(celebrity, one);		
	}

	@Test
	public void testCelebrityFoundIsPersontwoWhenEveryoneKnowsItAndItDoesntKnowByAnybody() {
		one.addNewKnownPerson(two);
		three.addNewKnownPerson(two);
		four.addNewKnownPerson(two);

		Person celebrity = app.findCelebrity(allPeoplelist);
				
		Assert.assertEquals(celebrity, two);		
	}

	private App app;
	private Set<Person> allPeoplelist;
	private Person one = new Person(1);
	private Person two = new Person(2);
	private Person three = new Person(3);
	private Person four = new Person(4);

	@Before
	public void initObjects() {
		app = new App();
		allPeoplelist = new HashSet<>();

		one = new Person(1);
		two = new Person(2);
		three = new Person(3);
		four = new Person(4);

		allPeoplelist.add(one);
		allPeoplelist.add(two);
		allPeoplelist.add(three);
		allPeoplelist.add(four);
	}

}
