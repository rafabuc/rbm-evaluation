package com.globant.test.evaluation;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;


/**
 * @author 
 * 
 */
public class Person implements Serializable{
	
	private static final long serialVersionUID = -2081039126726674926L;

	private Integer id;
	
	private Set<Person>  knownPeopleList = new HashSet<>() ;
				
	public Person(Integer id) {
		super();			
		this.id = id;			
	}
	
	public void clearknownPeople() {
		this.knownPeopleList.clear();
	}
	
	public boolean meet(Person person) {			
		return this.knownPeopleList.contains(person);			
	}

	public void addNewKnownPerson(Person person) {
		this.knownPeopleList.add(person);
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Set<Person> getKnownPeopleList() {
		return knownPeopleList;
	}

	public void setKnownPeopleList(Set<Person> knownPeopleList) {
		this.knownPeopleList = knownPeopleList;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Person other = (Person) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
					
		return true;
	}

	@Override
	public String toString() {
		return "Person [ id=" + id + "]";
	}

}
