package com.globant.test.evaluation;

import java.util.Set;

public class App {

	/**
	 * @param allPeopleList list of 
	 * @return  <li>{@link= Person} object if a celebrity is founded</li>
	 * <li> null if celebrity is not founded</li>
	 */
	public Person findCelebrity(Set<Person> allPeopleList) {

		for (Person person : allPeopleList) {			
			if (isCelebrity(person, allPeopleList)) {
				return person;
			}
		}

		return null;
	}

	private boolean isCelebrity(Person posiblecelebrity, Set<Person> allPeopleList) {

		boolean isknownByEveryone = isKnownByEveryone(posiblecelebrity, allPeopleList);
		boolean doesntKnownByAnybody = doesntKnownByAnybody( posiblecelebrity.getKnownPeopleList());

		if (isknownByEveryone && doesntKnownByAnybody) {
			return true;
		}
		return false;
	}

	private boolean isKnownByEveryone(Person posibleCelebrity, Set<Person> allPeople) {

		for (Person x : allPeople) {
			if (!x.equals(posibleCelebrity) && !x.meet(posibleCelebrity) ) {//
				return false;
			}
		}
		return true;
	}

	private boolean doesntKnownByAnybody(Set<Person> peopleKnowBythisPerson) {

		return peopleKnowBythisPerson.isEmpty();
	}

}
